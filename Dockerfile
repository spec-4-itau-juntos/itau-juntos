FROM nginx:1.17.1-alpine
COPY /dist/itau-juntos /usr/share/nginx/html/itau-juntos-webview
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf