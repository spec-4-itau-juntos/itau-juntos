import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjetoService } from '../services/projeto-service/projeto.service';
import {Projeto} from '../model/projeto.model';
import { Organizacao } from '../model/organizacao.model';
import { OrganizacaoService } from '../services/organizacao-service/organizacao.service';

@Component({
  selector: 'app-organizacao',
  templateUrl: './organizacao.component.html',
  styleUrls: ['./organizacao.component.css']
})
export class OrganizacaoComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private projetoService: ProjetoService,
    private organizacaoService: OrganizacaoService,
  ) {
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
  });
   }

   id: number;
   projetos: Projeto[];
   organizacao: Organizacao;
   loading = false;
   error = false;

  ngOnInit(): void {
    this.loading = true;
    this.consultarProjetosPorOrganizacao();
    this.consultarOrganizacoes();
  }

  consultarProjetosPorOrganizacao() {
    this.projetoService.getProjetos(this.id).subscribe(
      data => {
        console.log(data);
        this.projetos = data;
      },
      error => {
        console.log(error.toString());
        this.error = true;
      }
    );
  }

  consultarOrganizacoes() {
    this.organizacaoService.getOrganizacao(this.id).subscribe(
      data => {
        this.organizacao = data;
        this.loading = false;
      },
      error => {
        console.log(error.toString());
        this.error = true;
        this.loading = false;
      }
    );
  }



}
