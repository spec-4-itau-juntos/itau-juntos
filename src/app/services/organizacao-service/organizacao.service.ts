import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Organizacao } from 'src/app/model/organizacao.model';

@Injectable({
  providedIn: 'root'
})
export class OrganizacaoService {

  constructor(
    private http: HttpClient
  ) { }

  url = 'http://grupo2.spec4.mastertech.com.br/itaujuntos/dev/organizacao/v1/organizacao';


  getOrganizacoes() {
    return this.http.get<Organizacao[]>(this.url);
  }

  getOrganizacao(id: number) {
    return this.http.get<Organizacao>(this.url + '/' + id);
  }
}
