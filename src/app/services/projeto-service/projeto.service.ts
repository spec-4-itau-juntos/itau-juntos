import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Projeto } from 'src/app/model/projeto.model';

@Injectable({
  providedIn: 'root'
})
export class ProjetoService {

  constructor(
    private http: HttpClient
  ) { }

  url = 'http://grupo2.spec4.mastertech.com.br/itaujuntos/dev/projetos/v1/organizacao/';

  getProjetos(id: number) {
    return this.http.get<Projeto[]>(this.url + id + '/projeto');
  }
}
