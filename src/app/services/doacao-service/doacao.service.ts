import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Doacao } from '../../model/doacao.model';

@Injectable({
  providedIn: 'root'
})
export class DoacaoService {

  constructor(
    private http: HttpClient
  ) { }

  url = 'http://grupo2.spec4.mastertech.com.br/itaujuntos/doacao/v1/doacao';

  doarParaCampanha(doacao: Doacao) {
    return this.http.post(this.url, doacao);
  }
}
