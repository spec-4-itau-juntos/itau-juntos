import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CampanhaAtiva } from 'src/app/model/campanhaAtiva.model';

@Injectable({
  providedIn: 'root'
})
export class CampanhaService {

  constructor(
    private http: HttpClient
  ) { }

  url = 'http://grupo2.spec4.mastertech.com.br/itaujuntos/campanha/v1/campanha';

  getCampanhasAtivas() {
    return this.http.get<CampanhaAtiva[]>(this.url);
  }

  getCampanha(id: number) {
    return this.http.get<CampanhaAtiva>(this.url + '/' + id);
  }

}
