export class CampanhaAtiva {
    id: number;
    id_projeto: number;
    nome: string;
    descricao: string;
    data_inicio: Date;
    data_fim: Date;
    meta: number;
    situacao: number;
    valor_total: number;
    percentual: string;
}