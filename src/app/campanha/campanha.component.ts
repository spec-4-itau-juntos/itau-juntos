import { Component, Inject, OnInit } from '@angular/core';
import {CampanhaService} from '../services/campanha-service/campanha.service';
import {OrganizacaoService} from '../services/organizacao-service/organizacao.service';
import {CampanhaAtiva} from '../model/campanhaAtiva.model';
import { MatRadioChange } from '@angular/material/radio';
import { Organizacao } from '../model/organizacao.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjetoService } from '../services/projeto-service/projeto.service';
import { DoacaoService } from '../services/doacao-service/doacao.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Doacao } from '../model/doacao.model';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-campanha',
  templateUrl: './campanha.component.html',
  styleUrls: ['./campanha.component.css']
})
export class CampanhaComponent implements OnInit {

  constructor(
    private router: Router,
    private projetoService: ProjetoService,
    private doacaoService: DoacaoService,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private campanhaService: CampanhaService,
  ) {
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
    });
   }

  campanha: CampanhaAtiva;
  loading = false;
  error = false;
  id: number;

    doacaoInput = new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]*$')
    ]);

  ngOnInit(): void {
    this.loading = true;
    this.consultarCampanhas();

  }

  doar() {
    this.loading = true;
    this.error = false;

    let doacao: Doacao = {
      cpf_doador: '52325362060',
      valor: this.doacaoInput.value,
      id_campanha: this.id,
      data_doacao: new Date().toLocaleDateString()
    };

    console.log(doacao);
    this.doacaoService.doarParaCampanha(doacao).subscribe(
      data => {
        console.log(data);
        this.loading = false;
        this.openDialog();
      },
      error => {
        console.log(error.toString());
        this.error = true;
        this.loading = false;
      }
    );

  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogContent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  consultarCampanhas() {
    this.campanhaService.getCampanha(this.id).subscribe(
      data => {
        this.campanha = data;
        this.loading = false;
      },
      error => {
        console.log(error.toString());
        this.error = true;
        this.loading = false;
      }
    );
  }

  getPercentual(percentual: string): number {
    let formatado = percentual.split('.');
    return Number(formatado[0]);
  }

}

@Component({
  selector: 'dialog-content',
  templateUrl: 'dialog-content.html',
})
export class DialogContent {

  constructor(
    public dialogRef: MatDialogRef<DialogContent>,
    private router: Router,
  ) {

  }

  redirectHome() {
    this.dialogRef.close();
    this.router.navigate(['home']);
  }

}
