import { Component, OnInit } from '@angular/core';
import { MatRadioChange } from '@angular/material/radio';
import { Router } from '@angular/router';
import { CampanhaAtiva } from '../model/campanhaAtiva.model';
import { Organizacao } from '../model/organizacao.model';
import { CampanhaService } from '../services/campanha-service/campanha.service';
import { OrganizacaoService } from '../services/organizacao-service/organizacao.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private router: Router,
    private campanhaService: CampanhaService,
    private organizacaoService: OrganizacaoService,
  ) { }

  campanhasAtivas: CampanhaAtiva[];
  organizacoes: Organizacao[];
  opcao = 'Campanha';
  opcoes: string[] = ['Campanha', 'Organização'];
  loading = true;
  error = false;

  campanha = false;
  organizacao = false;
  projeto = false;

  ngOnInit(): void {
    this.consultarCampanhas();
  }

  radioChange(event: MatRadioChange) {
    this.campanha = false;
    this.organizacao = false;
    this.projeto = false;
    this.loading = true;
    this.error = false;
    if (event.value === 'Organização') {
      this.consultarOrganizacoes();
    } else if (event.value === 'Campanha') {
      this.consultarCampanhas();
    }
  }

  consultarOrganizacoes() {
    this.organizacaoService.getOrganizacoes().subscribe(
      data => {
        this.organizacoes = data;
        this.organizacao = true;
        this.loading = false;
      },
      error => {
        console.log(error.toString());
        this.error = true;   
        this.loading = false;
      }
    );
  }

  consultarCampanhas() {
    this.campanhaService.getCampanhasAtivas().subscribe(
      data => {
        this.campanhasAtivas = data;
        this.campanha = true;
        this.loading = false;
      },
      error => {
        console.log(error.toString());
        this.error = true;
        this.loading = false;
      }
    );
  }

  irParaPaginaOrganizacao(id: number) {
    this.router.navigate(['organizacao'], {queryParams: {'id': id}});
    
  }

  irParaPaginaCampanha(id: number) {
    this.router.navigate(['campanha'], {queryParams: {'id': id}});
  }

  getPercentual(percentual: string): number {
    let formatado = percentual.split('.');
    return Number(formatado[0]);
  }

}
